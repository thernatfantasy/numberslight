package com.thernat.numberslight

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
interface BasePresenter<T> {

    fun takeView(view: T)

    fun dropView()
}