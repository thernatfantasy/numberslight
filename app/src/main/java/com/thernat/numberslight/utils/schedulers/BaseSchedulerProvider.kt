package com.thernat.numberslight.utils.schedulers

import io.reactivex.Scheduler

/**
 * Created by m.rafalski@matsuu.com on 19/05/2019.
 */
interface BaseSchedulerProvider {

    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler
}