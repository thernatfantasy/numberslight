package com.thernat.numberslight.data.source

import com.thernat.numberslight.data.Number
import io.reactivex.Flowable

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
interface NumbersDataSource {

    fun getNumbers(): Flowable<List<Number>>

}