package com.thernat.numberslight.data.di

import com.thernat.numberslight.api.ApiService
import com.thernat.numberslight.data.source.NumbersDataSource
import com.thernat.numberslight.data.source.remote.NumbersRemoteDataSource
import com.thernat.numberslight.data.source.remote.Remote
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Module
class NumbersRepositoryModule {

    @Singleton
    @Provides
    @Remote
    internal fun provideNumbersRemoteDataSource(apiService: ApiService): NumbersDataSource {
        return NumbersRemoteDataSource(apiService)
    }
}