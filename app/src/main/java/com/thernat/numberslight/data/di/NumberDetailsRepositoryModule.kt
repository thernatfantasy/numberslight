package com.thernat.numberslight.data.di

import com.thernat.numberslight.api.ApiService
import com.thernat.numberslight.data.source.NumberDetailsDataSource
import com.thernat.numberslight.data.source.remote.NumberDetailsRemoteDataSource
import com.thernat.numberslight.data.source.remote.Remote
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Module
class NumberDetailsRepositoryModule {

    @Singleton
    @Provides
    @Remote
    internal fun prodiveNumberDetailsRemoteDataSource(apiService: ApiService): NumberDetailsDataSource {
        return NumberDetailsRemoteDataSource(apiService)
    }
}