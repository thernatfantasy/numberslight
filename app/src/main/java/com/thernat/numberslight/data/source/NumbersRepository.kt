package com.thernat.numberslight.data.source

import com.thernat.numberslight.data.Number
import com.thernat.numberslight.data.source.remote.Remote
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Singleton
class NumbersRepository@Inject constructor(@Remote private val remoteDataSource: NumbersDataSource):
    NumbersDataSource {

    private var cachedNumbers: List<Number> = listOf()

    override fun getNumbers(): Flowable<List<Number>> {
        if(cachedNumbers.isNotEmpty()){
            return Flowable.fromArray(cachedNumbers)
        }
        return getNumbersFromRemoteDataSourceAndCacheThem()
    }

    private fun getNumbersFromRemoteDataSourceAndCacheThem(): Flowable<List<Number>> {
        return remoteDataSource
            .getNumbers()
            .flatMap {
                numbers -> refreshCache(numbers)
                Flowable.fromArray(numbers)
            }
    }

    private fun refreshCache(numbers: List<Number>) {
        cachedNumbers =  numbers
    }
}