package com.thernat.numberslight.data.source.remote

import com.thernat.numberslight.api.ApiService
import com.thernat.numberslight.data.NumberDetails
import com.thernat.numberslight.data.source.NumberDetailsDataSource
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Singleton
class NumberDetailsRemoteDataSource @Inject constructor(private val apiService: ApiService): NumberDetailsDataSource {

    override fun getNumberDetails(name: String): Flowable<NumberDetails> {
        return apiService.getNumberDetails(name)
    }


}