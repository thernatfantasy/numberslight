package com.thernat.numberslight.data

data class NumberDetails(
    val image: String,
    val name: String,
    val text: String
)