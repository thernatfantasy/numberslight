package com.thernat.numberslight.data.source

import com.thernat.numberslight.data.NumberDetails
import io.reactivex.Flowable

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
interface NumberDetailsDataSource {

    fun getNumberDetails(name: String): Flowable<NumberDetails>
}