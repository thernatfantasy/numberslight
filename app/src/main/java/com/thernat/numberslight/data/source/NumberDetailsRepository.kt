package com.thernat.numberslight.data.source

import com.thernat.numberslight.data.NumberDetails
import com.thernat.numberslight.data.source.remote.Remote
import dagger.Component
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Singleton
class NumberDetailsRepository @Inject constructor(@Remote private val remoteDataSource: NumberDetailsDataSource): NumberDetailsDataSource {

    private var cachedNumberDetailsList: LinkedHashMap<String,NumberDetails> = LinkedHashMap()


    override fun getNumberDetails(name: String): Flowable<NumberDetails> {
        val numberDetailsInCache: NumberDetails? = cachedNumberDetailsList[name]
        if(numberDetailsInCache != null){
            return Flowable.just(numberDetailsInCache)
        }
        return getAndCacheRemoteDetail(name)
    }

    private fun getAndCacheRemoteDetail(name: String): Flowable<NumberDetails> {
        return remoteDataSource
            .getNumberDetails(name)
            .flatMap {numberDetails ->
                cacheNumberDetails(name,numberDetails)
                Flowable.just(numberDetails)
            }
    }

    private fun cacheNumberDetails(name: String, numberDetails: NumberDetails) {
        cachedNumberDetailsList[name] = numberDetails
    }


}