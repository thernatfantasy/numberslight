package com.thernat.numberslight.data

data class Number(
    val image: String,
    val name: String
)