package com.thernat.numberslight.data.source.remote

import javax.inject.Qualifier

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Remote