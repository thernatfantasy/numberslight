package com.thernat.numberslight.data.source.remote

import com.thernat.numberslight.api.ApiService
import com.thernat.numberslight.data.Number
import com.thernat.numberslight.data.NumberDetails
import com.thernat.numberslight.data.source.NumberDetailsDataSource
import com.thernat.numberslight.data.source.NumbersDataSource
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Singleton
class NumbersRemoteDataSource @Inject constructor(private val apiService: ApiService): NumbersDataSource {

    override fun getNumbers(): Flowable<List<Number>> {
        return apiService.getNumbers()
    }


}