package com.thernat.numberslight.api

import com.thernat.numberslight.data.Number
import com.thernat.numberslight.data.NumberDetails
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
interface ApiService {

    @GET("json.php")
    fun getNumbers(): Flowable<List<Number>>

    @GET("json.php")
    fun getNumberDetails(@Query("name")name : String): Flowable<NumberDetails>

}