package com.thernat.numberslight.api;

import com.thernat.numberslight.BuildConfig;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import javax.inject.Singleton;

/**
 * Created by m.rafalski@matsuu.com on 19/05/2019.
 */
@Module
public class NetModule {

    private static final String SERVICE_BASE_URL = "http://dev.tapptic.com/test/";

    @Provides
    @Singleton
    ApiService provideApiService(){
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVICE_BASE_URL)
                .build()
                .create(ApiService.class);
    }

    private OkHttpClient createOkHttpClient(){
        if(BuildConfig.LOG_HTTP) {
            return createOkHttpClientWithLoggingInterceptor();
        }
        return new OkHttpClient.Builder()
                .build();
    }

    private OkHttpClient createOkHttpClientWithLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }


}
