package com.thernat.numberslight.main.master

import com.thernat.numberslight.data.source.NumbersRepository
import com.thernat.numberslight.utils.schedulers.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
class NumbersListPresenter @Inject constructor(private val numbersRepository: NumbersRepository, private val schedulerProvider: SchedulerProvider) :
    NumbersListContract.Presenter {

    private var view: NumbersListContract.View? = null

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()


    override fun takeView(view: NumbersListContract.View) {
        this.view = view
        loadNumbersList()
    }

    private fun loadNumbersList() {
        view?.showLoading(true)
         numbersRepository
            .getNumbers()
            .subscribeOn(schedulerProvider.computation())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { view?.displayNumbers(it)},
                { t ->
                   view?.displayError()},
                {view?.showLoading(false)}
            ).apply {
                 compositeDisposable.add(this) }
    }


    override fun dropView() {
        this.view = null
        compositeDisposable.clear()
    }
}
