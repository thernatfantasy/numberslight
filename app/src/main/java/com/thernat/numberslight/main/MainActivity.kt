package com.thernat.numberslight.main

import android.os.Bundle
import android.util.Log
import com.thernat.numberslight.R
import com.thernat.numberslight.main.detail.NumberDetailFragment
import com.thernat.numberslight.main.master.NumbersListFragment
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), NumbersListFragment.OnElementSelectedListener {

    companion object {
        const val SELECTED_ELEMENT_NAME_BUNDLE_KEY = "selected_name"
    }

    @Inject
    lateinit var masterFragment: NumbersListFragment

    @Inject
    lateinit var detailFragment: NumberDetailFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(isTwoPane()){
            displayLandscapeView()
        } else {
            displayPortraitView()
        }

    }


    private fun displayLandscapeView(){
        val numberListFragment = supportFragmentManager.findFragmentById(R.id.frameMaster)
        val numberDetailFragment = supportFragmentManager.findFragmentById(R.id.frameDetail)
        if(numberListFragment == null){
            addFragmentToActivity(R.id.frameMaster,masterFragment)
        }else {
            replaceFragment(R.id.frameMaster,masterFragment)
        }
        if(numberDetailFragment == null){
            addFragmentToActivity(R.id.frameDetail,detailFragment)
        } else {
            replaceFragment(R.id.frameDetail,detailFragment)
        }
    }

    private fun displayPortraitView() {
        val numberListFragment = supportFragmentManager.findFragmentById(R.id.frameContent)
        if(numberListFragment == null){
            addFragmentToActivity(R.id.frameContent,masterFragment)
        }

    }

    private fun addFragmentToActivity(viewId: Int, fragment: DaggerFragment){
        supportFragmentManager.beginTransaction()
            .add(viewId,fragment)
            .commit()
    }


    private fun isTwoPane(): Boolean{
        return resources.getBoolean(R.bool.is_tablet)
    }

    override fun onElementSelected(name: String) {
        if(!isTwoPane()){
            Bundle().apply {
                putString(SELECTED_ELEMENT_NAME_BUNDLE_KEY,name)
                detailFragment.arguments = this
            }
            replaceFragment(R.id.frameContent,detailFragment)
        } else {
            detailFragment.numberSelected(name)
        }
    }

    private fun replaceFragment(viewId: Int, fragment: DaggerFragment){
        supportFragmentManager.beginTransaction()
            .replace(viewId,fragment)
            .addToBackStack(null)
            .commit()
    }
}
