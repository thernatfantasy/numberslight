package com.thernat.numberslight.main.detail

import com.thernat.numberslight.BasePresenter
import com.thernat.numberslight.BaseView
import com.thernat.numberslight.data.NumberDetails

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
interface NumberDetailContract {

    interface View : BaseView<Presenter> {

        fun showLoading(showLoading: Boolean)
        fun displayNumberDetails(numberDetails: NumberDetails)
        fun displayError()
    }

    interface Presenter : BasePresenter<View> {

         fun refreshNumber(newNumber: String)

    }
}