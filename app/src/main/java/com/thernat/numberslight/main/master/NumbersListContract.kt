package com.thernat.numberslight.main.master

import com.thernat.numberslight.BasePresenter
import com.thernat.numberslight.BaseView
import com.thernat.numberslight.data.Number

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
interface NumbersListContract {

    interface View : BaseView<Presenter> {

        fun displayNumbers(numbers: List<Number>)

        fun displayError()

        fun showLoading(show: Boolean)


    }

    interface Presenter : BasePresenter<View> {

    }

}