package com.thernat.numberslight.main.master

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thernat.numberslight.di.ActivityScoped
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import com.thernat.numberslight.R
import com.thernat.numberslight.data.Number
import com.thernat.numberslight.main.master.adapter.NumbersAdapter
import kotlinx.android.synthetic.main.fragment_master.*
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import io.reactivex.disposables.Disposable


/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@ActivityScoped
class NumbersListFragment @Inject constructor(): DaggerFragment(),NumbersListContract.View,NumbersAdapter.OnNumberClickedListener {

    @Inject
    lateinit var presenter: NumbersListContract.Presenter

    private lateinit var numbersAdapter: NumbersAdapter

    private var onElementSelectedListener: OnElementSelectedListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        numbersAdapter = NumbersAdapter()
        return inflater.inflate(R.layout.fragment_master, container,false)
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setAdapterOnRecyclerView()
        addDividerToRecyclerView()
    }

    private fun setAdapterOnRecyclerView() {
        numbersAdapter.onNumberClickedListener = this
        rvNumbers.adapter = numbersAdapter
    }

    private fun addDividerToRecyclerView() {
        val dividerItemDecoration = DividerItemDecoration(
           requireContext(),LinearLayoutManager.VERTICAL
        )
        rvNumbers.addItemDecoration(dividerItemDecoration)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnElementSelectedListener) {
            onElementSelectedListener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnElementSelectedListener.")
        }
    }

    override fun displayNumbers(numbers: List<Number>) {
        numbersAdapter.replaceData(numbers)
    }

    override fun displayError() {
    }

    override fun showLoading(show: Boolean) {
    }


    override fun onDestroy() {
        presenter.dropView()
        super.onDestroy()
    }

    override fun numberClicked(number: String) {
        onElementSelectedListener?.onElementSelected(number)
    }

    interface OnElementSelectedListener{

        fun onElementSelected(name: String)

    }
}