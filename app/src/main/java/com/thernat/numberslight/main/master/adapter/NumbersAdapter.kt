package com.thernat.numberslight.main.master.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.thernat.numberslight.data.Number
import com.thernat.numberslight.R
import kotlinx.android.synthetic.main.list_item_number.view.*

/**
 * Created by m.rafalski@matsuu.com on 19/05/2019.
 */
class NumbersAdapter  : RecyclerView.Adapter<NumbersAdapter.NumberViewHolder>(){

    private var numbers: List<Number> = listOf()

    var onNumberClickedListener: OnNumberClickedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberViewHolder {
        return NumberViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_number,parent,false))

    }

    override fun getItemCount(): Int {
        return numbers.size
    }

    override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
        val number : Number = numbers[position]
        holder.tvName.text = number.name
        loadImage(holder.ivNumber,number.image)
        holder.clRowNumber.setOnClickListener { onNumberClickedListener?.numberClicked(number.name) }
    }

    private fun loadImage(ivNumber: ImageView,url: String) {
        Picasso.get().load(url).into(ivNumber)
    }

    class NumberViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val clRowNumber : ConstraintLayout = view.clListRoot
        val ivNumber : ImageView = view.ivImage
        val tvName: TextView = view.tvName

    }

    fun replaceData(numbers: List<Number>){
        this.numbers = numbers
        notifyDataSetChanged()
    }

    interface OnNumberClickedListener{

        fun numberClicked(number: String)
    }
}