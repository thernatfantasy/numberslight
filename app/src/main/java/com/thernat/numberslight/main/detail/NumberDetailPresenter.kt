package com.thernat.numberslight.main.detail

import com.thernat.numberslight.data.source.NumberDetailsRepository
import com.thernat.numberslight.utils.schedulers.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
class NumberDetailPresenter @Inject constructor(private val numberDetailsRepository: NumberDetailsRepository, private val schedulerProvider: SchedulerProvider): NumberDetailContract.Presenter {

    private var view: NumberDetailContract.View? = null

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var selectedNumber: String? = null

    override fun takeView(view: NumberDetailContract.View) {
        this.view = view
        selectedNumber?.let { loadNumberDetails(it)}
    }

    private fun loadNumberDetails(number: String) {
        view?.showLoading(true)
        numberDetailsRepository
            .getNumberDetails(number)
            .subscribeOn(schedulerProvider.computation())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { view?.displayNumberDetails(it)},
                { t ->  view?.displayError()},
                {view?.showLoading(false)}
            ).apply {
                compositeDisposable.add(this) }
    }

    override fun refreshNumber(newNumber: String) {
        loadNumberDetails(newNumber)
        selectedNumber = newNumber
    }

    override fun dropView() {
        this.view = null
        compositeDisposable.clear()
    }

}