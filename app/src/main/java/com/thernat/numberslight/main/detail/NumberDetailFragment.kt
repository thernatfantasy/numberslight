package com.thernat.numberslight.main.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.thernat.numberslight.R
import com.thernat.numberslight.data.NumberDetails
import com.thernat.numberslight.di.ActivityScoped
import com.thernat.numberslight.main.MainActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@ActivityScoped
class NumberDetailFragment @Inject constructor(): DaggerFragment(),NumberDetailContract.View {


    @Inject
    lateinit var presenter: NumberDetailContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container,false)
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
        arguments?.getString(MainActivity.SELECTED_ELEMENT_NAME_BUNDLE_KEY)?.let{
            presenter.refreshNumber(it)
        }
    }


    override fun onDestroy() {
        presenter.dropView()
        super.onDestroy()
    }

    override fun displayNumberDetails(numberDetails: NumberDetails) {
        tvJName.text = numberDetails.text
        Picasso.get().load(numberDetails.image).into(ivImage)
    }


    override fun showLoading(showLoading: Boolean) {
    }

    override fun displayError() {

    }

    fun numberSelected(number: String) {
        presenter.refreshNumber(number)
    }
}