package com.thernat.numberslight.main

import com.thernat.numberslight.di.ActivityScoped
import com.thernat.numberslight.di.FragmentScoped
import com.thernat.numberslight.main.detail.NumberDetailContract
import com.thernat.numberslight.main.detail.NumberDetailFragment
import com.thernat.numberslight.main.detail.NumberDetailPresenter
import com.thernat.numberslight.main.master.NumbersListContract
import com.thernat.numberslight.main.master.NumbersListFragment
import com.thernat.numberslight.main.master.NumbersListPresenter
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Module
abstract class MainActivityModule {

    @ActivityScoped
    @Binds
    internal abstract fun numbersListPresenter(presenter: NumbersListPresenter): NumbersListContract.Presenter

    @ActivityScoped
    @Binds
    internal abstract fun numbersDetailPresenter(presenter: NumberDetailPresenter): NumberDetailContract.Presenter

    @FragmentScoped
    @ContributesAndroidInjector()
    internal abstract fun numbersListFragment(): NumbersListFragment

    @FragmentScoped
    @ContributesAndroidInjector()
    internal abstract fun numberDetailFragment(): NumberDetailFragment



}