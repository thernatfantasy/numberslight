package com.thernat.numberslight.di

import android.content.Context
import com.thernat.numberslight.NumbersLightApplication
import dagger.Binds
import dagger.Module

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Module
abstract class AppModule{

    @Binds
    internal abstract fun bindContext(application: NumbersLightApplication): Context

}