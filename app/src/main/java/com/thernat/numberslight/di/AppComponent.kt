package com.thernat.numberslight.di

import com.thernat.numberslight.NumbersLightApplication
import com.thernat.numberslight.api.NetModule
import com.thernat.numberslight.data.di.NumberDetailsRepositoryModule
import com.thernat.numberslight.data.di.NumbersRepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    ActivityBindingModule::class,AppModule::class,NetModule::class, NumbersRepositoryModule::class, NumberDetailsRepositoryModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: NumbersLightApplication): Builder

        fun build(): AppComponent
    }

    abstract fun inject(application: NumbersLightApplication)

}