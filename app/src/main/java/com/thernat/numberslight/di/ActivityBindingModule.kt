package com.thernat.numberslight.di

import com.thernat.numberslight.main.MainActivity
import com.thernat.numberslight.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by m.rafalski@matsuu.com on 18/05/2019.
 */
@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    internal abstract fun bindsMainActivity(): MainActivity
}